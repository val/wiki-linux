# Les bases du terminal

Par terminal, on entend plus spécifiquement _émulateur de terminal_.
Pour mieux comprendre la nomination, il faut faire un court voyage dans
le temps :

> Avant l'arrivée des ordinateurs individuels (PC) sur les bureaux,
> jusque dans les années 1980, le coût et la place requise pour un
> ordinateur étaient tels que l'ordinateur était dans une pièce et les
> périphériques dans une autre. Le terminal était un clavier et un écran
> reliés à l'ordinateur distant, et plusieurs utilisateurs utilisaient le
> même ordinateur en partage. On peut voir le terminal un peu comme un
> ordinateur sans mémoire ni capacité de stockage et avec juste une carte
> réseau, à l'image des boîtes d'accès à l'internet que l'on connecte sur
> une télévision. - Wikipedia

Donc, le terminal était un outil de communication entre l'ordinateur et
l'utilisateur. Dans un sens moderne, c'est un programme qui affiche des
caractères en traitant des commandes entrées avec un clavier.
Dans l'administration d'un système GNU/Linux, le terminal est un outil
puissant, pratique, rapide et adapté à de nombreuses situations.
La plus forte raison d'utilisation du terminal est qu'il permet
d'achever beaucoup de taches plus rapidement qu'en interface graphique,
et que parfois, quelques utilitaires ne sont disponibles qu'en
[CLI](lexique.html#cli).

Bref, Vous l'aurez compris, maitriser un minimum le terminal est
**indispensable**.

Notez que cet article propose uniquement lest bases des commandes
citées, pour plus d'options, consultez [la documentation](#man).

1.  [À lire avant de commencer.](#BAS)
    1. [Conventions d'écriture.](#CNV)
    1. [Chercher de l'aide ou des commandes.](#MAN)
1.  [Terminal comme gestionnaire de fichiers.](#TFM)
    1. [ls](#LS)
    1. [cd](#CD)
    1. [rm](#RM)
    1. [touch](#TC)
    1. [mkdir](#MD)
    1. [cp &amp; mv](#MC)
1.  [La gestion des processus.](#PROS)
    1. [Lancer un programme comme une tâche de fond.](#FOND)
    1. [Contrôler une tâche de fond.](#CFON)

* * *

## À lire avant de commencer

### Conventions d'écriture

Lors de ce tutoriel, nous utilisons des "indicateurs de droits" qui précèdent les commandes :

    $ = Utilisateur normal
    # = Super-Utilisateur

Il ne faut évidemment pas recopier ces symboles dans le terminal : la
commande ne renverra rien du tout car, dans le cas de `#`, le terminal
comprendra qu'il s'agit d'un commentaire n'ayant aucun incidence dans le
comportement de la commande et, dans le second cas, essayera
d'interpréter `$` comme une commande.

Les noms de logiciels dans les phrases seront écrit en `monospace`, les
arguments en **gras**, les variables en ***gras italique***, les noms de
fichiers en _italique_ et les commandes ou les fonctions seront écrite
dans des blocs, comme celui au dessus.

### Chercher de l'aide ou des commandes

Chaque système GNU/Linux possède un manuel qui décrit l'utilisation de
tous les programmes présents sur le système. Néanmoins, les pages de
manuel ne décrivent que rarement l'utilisation complète des commandes,
il vous faudra donc le lire attentivement et essayer par vous même ou
utiliser un site web capable d'interpréter les commandes. De plus, même
si les pages correspondantes aux programmes essentiels du système sont
traduite en français, ce n'est pas le cas de celles de certains petits
logiciels.

Vous pouvez ouvrir le manuel du manuel pour en comprendre le
fonctionnement de cette façon :

    $ man man

Un système contient énormément de commandes et il est difficile de
toutes les retenir. C'est pour cette raison qu'il existe plusieurs
méthodes pour retrouvez des commandes. L'une d'entre elle est, comme
vous devez le savoir après avoir lu le manuel du manuel, l'option **-k**
du manuel. Cette option est l'équivalent de la commande `apropos`.
`apropos` est une commande utilisant la base de donnée de `whatis` pour
faire ses recherches. `apropos` renvoie comme résultat les pages du
manuel correspondant à un terme précis :

    $ apropos manual
    compress []          (1)  - dummy - manual page for compress-dummy - sharutils 4.6.3
    man []               (1)  - format and display the on-line manual pages
    whereis []           (1)  - locate the binary, source, and manual page files for a command
    xman []              (1)  - Manual page display program for the X Window System

Vous n'avez plus qu'à lire le manuel de ce qui vous parrait correcte et
à l'utiliser.

Notez que vous pouvez utiliser l'autocompletiton de votre Terminal avec
la touche des _tabulations_. Elle peut completer les commandes, les
répertoires, les noms de scripts, etc.

* * *

## Terminal comme gestionnaire de fichiers

Quand vous ouvrez un gestionnaire de fichiers (Thunar, Dolphin, etc.),
les dossiers et les fichiers ainsi présents s'affichent. Quand vous
lancez votre terminal, vous êtes dans un certain dossier, généralement
dans le ***$HOME***, aussi connu comme étant ***~/*** ou
***/home/vous***.  Si vous ne savez pas qui ***vous*** êtes, entrez la
commande `whoami`.

### ls

Pour pouvoir gérer nos fichiers, il faut en premier pourvoir les voir.
Pour cela, nous utilisons la commande `ls`. La commande `ls`, pour `List
directiory contentS`, affiche le contenu du dossier actuel. Par défaut,
`ls` n'affiche pas tous les fichiers et dossiers présents, il n'affiche
que ceux qui sont visible. Pour afficher les fichiers et dossiers cachés
(ceux qui commencent par un « . »), il faut ajouter, comme écrit dans le
manuel, l'option **-a**, on aura donc `ls -a` :

    $ ls
    Documents Downloads script.sh chat.jpg
    $ ls -a
    Documents Downloads script.sh chat.jpg .config .local .cache

Avec certaines distributions, `ls` se lance directement avec l'option
**--color=auto** qui permet d'afficher les fichiers et les dossiers en
couleur pour mieux en faire la différence. Si elle ne fait pas ça par
défaut, vous devrez le rajouter :

<pre>$ ls --color=auto
<span style="color: blue;"><strong>Documents</strong> <strong>Downloads</strong></span> <span style="color: green;"><strong>script.sh</strong></span> <strong>chat.jpg</strong>
</pre>

### cd

Maintenant que nous sommes capables d'affichers les dossiers, nous
pouvons naviguer tranquillement. La commande `cd`, abréviation de
`change directory`, vous permet de changer de répertoire. Après avoir
entré la commande `ls` par exemple, vous souhaiterez passer vers un
répertoire listé, comme Documents :

<pre>$ ls --color=auto
<span style="color: blue;"><strong>Documents</strong> <strong>Downloads</strong></span> <span style="color: green;"><strong>script.sh</strong></span> <strong>chat.jpg</strong>
$ cd Documents
$ ls --color=auto
<strong>rapport_stage.txt</strong> <span style='color: red;'><strong>savoir_lire_savoir_ecrire.pdf</strong></span>
</pre>

Notez la présence, si vous affichez les dossiers cachés, de deux
répertoire en plus : ***.*** et ***..***. ***.*** correspond au dossier
courant et ***..*** au dossier antérieur. Si vous êtes perdu dans vos
répertoire utilisez simplement `pwd` :

    $ pwd
    /home/knakis/Documents

En utilisant simplement `cd`, vous reviendrez dans votre répertoire
utilisateur (ici ***/home/knakis***) et si vous utilisez `cd ..` vous
reviendrez au dossier précédent (ici aussi ***/home/knakis***).

### rm

La commande `rm`, intitulée `remove files or directories`, permet de
supprimer des fichiers et des dossiers à volonté. En étant à nouveau
dans notre dossier ***$HOME***, nous pouvons supprimer un fichier ou un
dossier listé.

**Soyez prudents lors de l'utilisation de cette commande**, la suppression d'un dossier ou d'un fichier avec cette commande n'est pas facilement une tache réversible.

    $ ls
    Documents Downloads script.sh chat.jpg
    $ rm script.sh
    $ ls
    Documents Downloads chat.jpg

Pour supprimer un dossier, il faut ajouter l'option **-r**. Notez bien
que tous les contenus du dossier que vous supprimerez seront supprimés
aussi.

    $ ls
    Documents Downloads chat.jpg
    $ rm Documents
    rm: cannot remove ‘Documents’: Is a directory
    $ rm -r Documents
    $ ls
    Downloads chat.jpg

### touch

La commande `touch` sert tout simplement à modifier le timestamp d'un
fichier, c'est-à-dire la date de modification. La principale utilisation
de `touch` vient du fait qu'il crée un fichier si celui-ci n'existait
pas, exemple :

    $ cd /home/knakis/
    $ touch fichier

Un fichier nommé "fichier" apparaît comme vous pourrez le constater.

Un fichier sans extension est normalement traité, à plus grande raison,
comme un fichier texte. Vous pouvez tout de même spécifier une extension
de fichier :

$ touch document.odt

Vous pouvez aussi créer des fichiers invisibles en ajoutant un point au
début du nom du fichier.

### mkdir

La commande `mkdir` sert tout simplement à créer un dossier.

Etant au répertoire ***$HOME*** par exemple, nous pourrons ajouter un
dossier.

    $ mkdir devoirs

En exécutant la commande `ls`, vous verrez, en plus des dossiers déjà
présents, ce nouveau dossier.

Vous pouvez aussi créer toute une arborescence d'un coup en utilisant
l'option **-p** :

    $ mkdir -p devoirs/maths/trigonometrie

créera les dossier **'devoirs**' et **'maths**' en plus de
**'trigonometrie**' si ils n'existaient pas.

Vous pouvez aussi créer des dossiers invisibles en ajoutant un point au
début du nom du dossier ou crée des dossiers avec des caractères
spéciaux, commes les espaces, en les précédents par un antislash
(**\**).

### cp et mv

`cp` pour _copy_ et `mv` pour _move_, sont deux commandes qui servent à
transmettre des fichiers ou des dossiers d'un répertoire à un autre.
`cp` fait une copie de l'élément à copier, tandis que `mv` le déplace.

L'utilisation de ces deux commandes est de la forme `cp`
***&lt;fichier_source&gt;*** ***&lt;destination&gt;***. Nous allons
illustrer quelques exemples ci-dessous, en considérant que nous sommes
dans le répertoire ***$HOME*** :

    $ cp document.odt devoirs

Dans cet exemple, nous avons copié "document.odt" au dossier "devoirs".

    $ cp images/graphe.jpg devoirs

Dans celui-ci, nous avons ajouté le répertoire où se trouve le fichier à
recopier, vu que "graphe.jpg" se trouve dans le dossier images.

    $ cp images/vdm.jpg devoirs/DM.jpg

Dans cet exemple-là, nous avons copié le fichier "vdm.jpg" tout en lui
donnant une nouvelle nomination : à sa destination, "vdm.jpg" sera
renommé "DM.jpg".

`cp` ne prend pas en compte les duplications, c'est à dire que si vous
avez un fichier nommé A et que vous le copiez vers une destination qui
contient déjà un fichier nommé A, ce dernier sera écrasé
automatiquement. Mais par ailleurs, vous pouvez contourner cette grave
erreur en ajoutant l'option **-i** :

    $ cp -i document.odt devoirs

Et vous serez ainsi prévenus avant l'écrasement.

Comme pour la commande `rm`, le recopiage de dossier nécessite
l'utilisation de l'option **-r**. Il s'agit tout à fait de même pour
`mv` si vous prenez en compte les bonnes logiques. Par ailleurs, cette
commande sert en dehors de son contexte réel, à renommer les fichiers et
les dossiers :

    $ mv devoirs projets

Cette commande renommera le dossier "devoirs" en "projets".

* * *

## La gestion de processus

Quand vous utilisez un programme dans votre Terminal il n'est pas rare
que celui-ci l'occupe entièrement, vous obligeant à attendre sa fin pour
réaliser d'autres actions. Vous apprendrez, dans cette section, à manier
les processus, c'est-à-dire, les programmes, les commandes.

### Lancer un programme comme tâche de fond

Comme vu précédemment, la commande `cp` permet de copier un fichier.
`cp` ne vous laissera accès à  votre terminal qu'après avoir fini le
copiage, vous mettant ainsi en attente. Il en est de même pour toutes
les commandes, un terminal vous permet une tâche à la fois. Affin d'y
remédier, il est possible de lancer une commande comme tâche de fond :
ajouter **&amp;** à la fin de la commande.

    $ cp party.mp4 ~/videos &amp;
    [1] 15217

Après avoir libéré la commande, son numéro de processus est fourni
automatiquement, vous permettant ainsi de l'interrompre avant qu'il ne
s'arrête si souhaité. La commande `kill` permet simplement d'arrêter un
processus.

    $ kill 15217

### Contrôler une tâche de fond

En utilisant **&amp;**, le contrôle d'une tâche de fond peut s'avérer
problématique, c'est pour cela qu'il est mieux conseillé d'utiliser la
commande `screen`.

    $ screen cp party.mp4 ~/videos

Puis appuyez sur Ctrl+A+D pour détacher le processus. C'est maintenant
exactement comme si vous aviez utilisé **&amp;**. Afin de le rattacher :

    $ screen -r
